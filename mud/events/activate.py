from .event import Event2

class ActivateEvent(Event2):
    NAME = "activate"
    
    def perform(self):
        if not self.object.has_prop("activable"):
            self.fail()
            return self.inform("activate.failed")
        self.inform("activate")
