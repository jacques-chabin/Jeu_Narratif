from .action import Action2
from mud.events import ActivateEvent

class ActivateAction(Action2):
    EVENT = ActivateEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "activate"
