from .effect import Effect3
from mud.events import AnswerEvent

class AnswerEffect(Effect3):
    EVENT = AnswerEvent
